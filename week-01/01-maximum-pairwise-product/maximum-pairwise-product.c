/* Find maximum pairwise product of a set of numbers
 *
 * Known input characteristics
 * 2 ≤ n ≤ 200,000
 * 0 ≤ n ≤ 200,000
 *
 */


#include <stdio.h>
#include <stdlib.h>


/*
 * The largest number in the test is 200,000 - this is 6 characters when
 * written out as a string. Together with a space, each number will take up
 * to 7 characters (excluding the first, which has no space). Moreover,
 * we can have up to 200000 numbers. So we need to have a minimum buffer
 * size 1.4 million (200,000 * 7). Which sounds like a lot,
 * but it's just 1.4 MB lol. The choice of buffer size tripped me up in one
 * of the automated tests. The perils of C, I suppose. Or at least the
 * perils of still programming like it's 1989.
*/
#define BUFSIZE (2048000 + 1) // A cool 2MB buffer, just for the heck of it

size_t get_next_number(char **currentptr, char **reentryptr) {
    size_t num = strtoll(*currentptr, reentryptr, 10);
    *currentptr = *reentryptr;
    return num;
}

int main() {

    char buffer[BUFSIZE] = "";

    size_t   n = 0;

    size_t   curr;     // Will store the current number being read.
    size_t   num1 = 0; // Stores the largest number so far.
    size_t   num2 = 0; // Stores the second largest number so far.

    // Get the number of elements.
    fgets(buffer, BUFSIZE, stdin);
    n = strtol(buffer, NULL, 10);

    // Get the line with all the numbers.
    fgets(buffer, BUFSIZE, stdin);

    // Get numbers one by one and collect the largest and second largest
    // numbers in the set.
    char *current = buffer;
    char *reentry = NULL;
    for (size_t i = 0; i < n; i++) {
        curr = get_next_number(&current, &reentry);
        if (curr > num1) {
            num2 = num1;
            num1 = curr;
        } else if (curr > num2) {
            num2 = curr;
        } else {
            NULL;
        }
    }
    printf("%zu\n", num1 * num2);

    return 0;
}
