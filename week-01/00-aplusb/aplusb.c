#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE (2048 + 1)

int main() {

  long long numbers[2];
  char buffer[BUFSIZE];

  // The following chunk will become a staple for us over the course on how
  // we read in data for the programming challenges.
  fgets(buffer, BUFSIZE, stdin);
  char *current = buffer;
  char *reentry = NULL;
  for (int i = 0; i < 2; i++) {
      numbers[i] = strtoll(current, &reentry, 10);
      current = reentry;
  }
  printf("%lld\n", numbers[0] + numbers[1]);

  return 0;
}
