#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#define BUFSIZE (2048000 + 1)

unsigned long pisano(unsigned long m) {
    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;

    if (m < 2) return m;

    for (unsigned long i = 0; i < m * m; i++) {
        tmp = (f_1 + f_2) % m;
        f_1 = f_2;
        f_2 = tmp;
        if ((f_1 == 0) && (f_2 == 1)) {
            return i + 1;
        }
    }
    return 1;
}

int main() {
    char buffer[BUFSIZE] = "";

    fgets(buffer, BUFSIZE, stdin);

    unsigned long N  = strtoull(buffer, NULL, 10);

    if (N < 2) {
        printf("%llu\n", N);
        return 0;
    }

    unsigned long m  = 10;
    unsigned long P  = pisano(m);

    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;

    unsigned long rs[P];
    rs[0] = f_1 % m;
    rs[1] = f_2 % m;
    unsigned long rsum = (0 + 1); // Remainder of first two elements;
                                  // Won't be used since in this
                                  // program since for mod 10, the
                                  // last digit is 0.
    for (unsigned long i = 2; i < P; i++) {
        tmp = f_1 + f_2;
        rsum = rsum + (tmp % m);
        rs[i] = tmp % m;
        f_1 = f_2;
        f_2 = tmp;
    }

    unsigned long n   = N % P;
    unsigned long nr  = N / P;

    unsigned long ans = 0;
    for (unsigned long i = 0; i <= n; i++) {
        ans = ans + rs[i];
    }
    printf("%llu\n", ans % m);

    return 0;
}
