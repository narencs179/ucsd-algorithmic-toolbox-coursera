#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE (2048000 + 1)


// Cute little recursive function. I first read about this in SICP.
size_t gcd(size_t num1, size_t num2) {
    size_t rem = num1 % num2;
    if (rem == 0) {
        return num2;
    } else {
        return gcd(num2, rem);
    }
}

int main() {
    char buffer[BUFSIZE];

    fgets(buffer, BUFSIZE, stdin);

    char *reentry = NULL;

    size_t num1 = strtoll(buffer, &reentry, 10);
    size_t num2 = strtoll(reentry, NULL, 10);

    printf("%zu\n", gcd(num1, num2));


    return 0;
}
