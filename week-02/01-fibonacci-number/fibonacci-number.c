#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE (2048000 + 1)

int main() {
    char buffer[BUFSIZE] = "";

    fgets(buffer, BUFSIZE, stdin);

    size_t n    = strtoll(buffer, NULL, 10);
    size_t f_1  = 0;
    size_t f_2  = 1;

    size_t ans;
    size_t tmp;

    if (n == 0) {
        ans = f_1;
    } else {
        for (size_t i = 1; i < n; i++) {
            tmp = f_1 + f_2;
            f_1 = f_2;
            f_2 = tmp;
        }
        ans = f_2;
    }
    printf("%zu\n", ans);


    return 0;
}
