#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#define BUFSIZE (2048000 + 1)


unsigned long pisano(unsigned long m) {
    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;

    if (m < 2) return m;

    for (unsigned long i = 0; i < m * m; i++) {
        tmp = (f_1 + f_2) % m;
        f_1 = f_2;
        f_2 = tmp;
        if ((f_1 == 0) && (f_2 == 1)) {
            return i + 1;
        }
    }
    return 1;
}

int main() {
    char buffer[BUFSIZE] = "";

    fgets(buffer, BUFSIZE, stdin);

    char *reentry = NULL;
    unsigned long N  = strtoull(buffer, &reentry, 10);
    unsigned long m  = strtoull(reentry, NULL, 10);

    unsigned long p  = pisano(m);
    unsigned long n  = N % p;

    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;
    unsigned long ans;

    if (n < 2) {
        ans = n;
    } else {
        for (unsigned long i = 1; i < n; i++) {
            tmp = (f_1 + f_2) % m;
            f_1 = f_2;
            f_2 = tmp;
        }
        ans = f_2 % m;
    }
    printf("%lu\n", ans);

    return 0;
}
