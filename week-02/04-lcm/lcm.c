#include <stdio.h>
#include <stdlib.h>
#define BUFSIZE (2048000 + 1)


size_t gcd(size_t num1, size_t num2) {
    size_t rem = num1 % num2;
    if (rem == 0) {
        return num2;
    } else {
        return gcd(num2, rem);
    }
}

// lcm(a, b) = (a * b) / gcd(a, b)
size_t lcm(size_t num1, size_t num2) {
    size_t g        = gcd(num1, num2);
    size_t new_num1 = (num1 / g);
    return new_num1 * num2;
}

int main() {
    char buffer[BUFSIZE];

    fgets(buffer, BUFSIZE, stdin);

    char *reentry = NULL;

    size_t num1 = strtoll(buffer, &reentry, 10);
    size_t num2 = strtoll(reentry, NULL, 10);

    printf("%zu\n", lcm(num1, num2));


    return 0;
}
