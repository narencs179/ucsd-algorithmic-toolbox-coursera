#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#define BUFSIZE (2048000 + 1)

unsigned long pisano(unsigned long m) {
    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;
if (m < 2) return m;

    for (unsigned long i = 0; i < m * m; i++) {
        tmp = (f_1 + f_2) % m;
        f_1 = f_2;
        f_2 = tmp;
        if ((f_1 == 0) && (f_2 == 1)) {
            return i + 1;
        }
    }
    return 1;
}

int main() {

    unsigned long m  = 10;
    unsigned long P  = pisano(m);

    unsigned long f_1 = 0;
    unsigned long f_2 = 1;
    unsigned long tmp;

    unsigned long rs[P];
    rs[0] = f_1 % m;
    rs[1] = f_2 % m;
    for (unsigned long i = 2; i < P; i++) {
        tmp = f_1 + f_2;
        rs[i] = tmp % m;
        f_1 = f_2;
        f_2 = tmp;
    }

    char buffer[BUFSIZE] = "";
    fgets(buffer, BUFSIZE, stdin);

    char *reentry = NULL;
    unsigned long N_1  = strtoul(buffer, &reentry, 10);
    unsigned long N_2  = strtoul(reentry, NULL, 10);

    // Case 1: The difference between the two limits are not large enough for
    // repetition to occur. Just calculate the sum normally.
    unsigned long ans = 0;
    if ((N_2 - N_1) < P) {
        for (unsigned long i = N_1; i <= N_2; i++) {
            ans = ans + rs[i % P];
        }
        printf("%lu\n", ans % 10);
        return 0;
    }


    // Case 2: Consider the sequence to be composed of up to three parts
    // left part   : Optional part that doesn't fit into the middle part.
    // middle part : Composed of multiples of P elements. Easy to calculate
    //               the sum of digits sum rsum
    // right part  : Optional part that doesn't fit into the middle part.
    //
    // The left and right parts together will be less than 2P elements
    // The middle part will be calculated very quickly, quicker than the side
    // parts.
    unsigned long A_1 = (N_1 + P) - (N_1 % P);
    unsigned long A_2 = N_2 - (N_2 % P);
    unsigned long nrep;
    nrep = (A_2 - A_1) / P;

    unsigned long left = 0;
    for (unsigned long i = N_1; i < A_1; i++) {
        left = left + rs[i % P];
    }
    left = left % 10;

    unsigned long right = 0;
    for (unsigned long i = (A_2 + 1); i <= N_2; i++) {
        right = right + rs[i % P];
    }
    right = right % 10;

    ans = right + left; // + (nrep * rsum); Not necessary since rsum last digitg is 0
    printf("%lu\n", ans % 10);

    return 0;
}
