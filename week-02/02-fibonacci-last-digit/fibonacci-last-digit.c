#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#define BUFSIZE (2048000 + 1)

/*
 * For n ≥ 1, the nth fibonacci number is defined by the sum of the (n-1) and
 * (n-2) fibonacci numbers. As a result, the last digit of the nth fibonacci
 * number can be calculated using the last digits of the (n-1) and the (n-2)
 * fibonacci numbers. This solution just keeps track of the last digits. 
 * Whenever the sum of the two last digits goes above 10, it rolls it back to
 * single digit to keep the solution on track.
 *
 */

int main() {
    char buffer[BUFSIZE] = "";

    fgets(buffer, BUFSIZE, stdin);

    size_t n    = strtoll(buffer, NULL, 10);
    int f_1  = 0;
    int f_2  = 1;

    int ans;
    int tmp;

    if (n == 0) {
        ans = f_1;
    } else {
        for (size_t i = 1; i < n; i++) {
            tmp = f_1 + f_2;
            if (tmp > 9) {
                tmp = tmp - 10;
            }
            f_1 = f_2;
            f_2 = tmp;
        }
        ans = f_2;
    }

    printf("%d\n", ans);

    return 0;
}
